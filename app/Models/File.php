<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

/**
 * App\Models\File
 *
 * @property string $uuid
 * @property int $owner_id
 * @property string $owner_name
 * @property string $owner_lastname
 * @property string $name
 * @property string $mime_type
 * @property int $download_count
 * @property int $size
 */
class File extends Model
{
    use HasFactory;

    public $incrementing = false;

    protected $keyType = 'string';
    protected $primaryKey = 'uuid';

    protected $fillable = [
        'uuid',
        'owner_id',
        'owner_name',
        'owner_lastname',
        'name',
        'mime_type',
        'download_count',
        'size'
    ];

    public function downloads()
    {
        return $this->hasMany(FileDownload::class, 'uuid', 'uuid');
    }
}
