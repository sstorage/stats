<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

/**
 * App\Models\File
 *
 * @property string $uuid
 * @property string $user_id
 */
class FileDownload extends Model
{
    public $incrementing = false;

    use HasFactory;

    protected $fillable = [
        'uuid',
        'user_id',
        'user_name',
        'user_lastname',
    ];
}
