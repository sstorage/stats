<?php

namespace App\Services;

use App\Http\Requests\GatewayRequest;
use GuzzleHttp\Psr7\Response;
use Illuminate\Http\Client\PendingRequest;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;

class MicroserviceClient
{
    /**
     * Получает информацию о пользователе из внутренней ручки микросервиса auth
     * @param string $userId
     * @return bool
     */
    public function getUserInfo(?string $userId): ?array
    {
        $configPath = 'microservices.auth';
        $url = $this->getUrl($configPath, 'internal.user_info', [$userId]);
        $headers = ['accept' => 'application/json'];
        /** @var Response $response */

        try {
            $response = \Http::withHeaders($headers)->get($url);
            $responseContent = $response->getBody()->getContents();
            $responseData = json_decode($responseContent, true);
            return $responseData['data'] ?? null;
        } catch (\Throwable $e) {
            \Log::error($e->getMessage());
        }

        return null;
    }

    public function getUrl(string $configPath, string $alias, array $urlParams = []): string
    {
        $url = config($configPath . '.' . $alias);
        return sprintf($url, ...$urlParams);
    }

}
