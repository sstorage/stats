<?php

namespace App\Jobs;

use App\Models\FileDownload;
use App\Services\MicroserviceClient;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class UpdateFileDownloadCount implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private string $fileUid;
    private ?string $userId;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $uid, ?string $userId = null)
    {
        $this->fileUid = $uid;
        $this->userId = $userId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(MicroserviceClient $client)
    {
        DB::table('files')
            ->where('uuid', $this->fileUid)
            ->increment('download_count', 1);

        $downloadData = [
            'uuid' => $this->fileUid,
        ];
        if (!empty($this->userId)) {
            $downloadData['user_id'] = $this->userId;
            $userInfo = $client->getUserInfo($this->userId);
            if (!empty($userInfo['name'])) {
                $downloadData['user_name'] = $userInfo['name'];
            }
            if (!empty($userInfo['lastname'])) {
                $downloadData['user_lastname'] = $userInfo['lastname'];
            }
        }
        FileDownload::create($downloadData);

        Log::info('File ' . $this->fileUid . ' downloaded');
    }
}
