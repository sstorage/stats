<?php

namespace App\Jobs;

use App\Models\File;
use App\Models\FileDownload;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class UpdateStatsAfterUserUpdate implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private array $userData;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $userData)
    {
        $this->userData = $userData;
        Log::info('Update user data' . print_r($this->userData, true));
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if (empty($this->userData['uuid'])) {
            return;
        }
        File::where('owner_id', $this->userData['uuid'])->update([
            'owner_name' => $this->userData['name'],
            'owner_lastname' => $this->userData['lastname'],
        ]);
        FileDownload::where('user_id', $this->userData['uuid'])->update([
            'user_name' => $this->userData['name'],
            'user_lastname' => $this->userData['lastname'],
        ]);
    }
}
