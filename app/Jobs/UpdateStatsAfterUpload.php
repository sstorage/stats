<?php

namespace App\Jobs;

use App\Models\File;
use App\Services\MicroserviceClient;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class UpdateStatsAfterUpload implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $fileData = [];

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $fileData)
    {
        $this->fileData = $fileData;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(MicroserviceClient $client)
    {
        Log::info(print_r($this->fileData, true));
        if (empty($this->fileData)) {
            return;
        }
        $file = File::create($this->fileData);
        if (empty($file->owner_id)) {
            return;
        }
        $userInfo = $client->getUserInfo($file->owner_id);
        if (!empty($userInfo['name'])) {
            $file->owner_name = $userInfo['name'];
        }
        if (!empty($userInfo['lastname'])) {
            $file->owner_lastname = $userInfo['lastname'];
        }
        $file->save();
    }
}
