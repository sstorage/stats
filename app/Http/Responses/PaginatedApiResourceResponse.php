<?php
namespace App\Http\Responses;

use Illuminate\Http\Resources\Json\PaginatedResourceResponse;
use Illuminate\Support\Arr;

class PaginatedApiResourceResponse extends PaginatedResourceResponse
{
    public $success = true;
    public $async = false;

    protected $notifications = [];


    /**
     * Add the pagination information to the response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    protected function paginationInformation($request)
    {
        $paginated = $this->resource->resource->toArray();

        return [
            'meta' => $this->meta($paginated),
        ];
    }

    /**
     * Gather the meta data for the response.
     *
     * @param  array  $paginated
     * @return array
     */
    protected function meta($paginated)
    {
        $paginated = parent::meta($paginated);
        //Удаляем links, мы их не используем
        if (isset($paginated['links'])) {
            unset($paginated['links']);
        }
        $paginationParams = [
            'current_page',
            'from',
            'path',
            'per_page',
            'to',
            'total',
            'last_page',
        ];
        $meta = Arr::except($paginated, $paginationParams);
        $meta['pagination'] = Arr::only($paginated, $paginationParams);
        $meta['success'] = $this->success;
        $meta['async'] = $this->async;
        return $meta;
    }
}
