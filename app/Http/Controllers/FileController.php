<?php

namespace App\Http\Controllers;

use App\Exceptions\UnableToSaveFileException;
use App\Http\Requests\GetFileDownloadsRequest;
use App\Http\Resources\ApiCollectionResource;
use App\Http\Resources\FilesCollectionResource;
use App\Models\File;
use App\Services\JwtService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class FileController extends Controller
{

    /**
     * @param Request $request
     * @return Response
     * @throws UnableToSaveFileException
     */
    public function index(Request $request): ApiCollectionResource
    {
        return new FilesCollectionResource(File::paginate($request->get('per_page')));
    }

    /**
     * @param Request $request
     * @return Response
     * @throws UnableToSaveFileException
     */
    public function userFiles(Request $request, JwtService $jwt): ApiCollectionResource
    {
        $userId = $jwt->getUserId();
        if (empty($userId)) {
            return new FilesCollectionResource([]);
        }
        return new FilesCollectionResource(
            File::where(['owner_id' => $userId])->paginate($request->get('per_page'))
        );
    }

    public function downloads(GetFileDownloadsRequest $request, File $file)
    {
        return new ApiCollectionResource($file->downloads()->paginate($request->get('per_page')));
    }
}
