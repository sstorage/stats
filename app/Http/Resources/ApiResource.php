<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ApiResource extends JsonResource
{
    public $success = true;
    public $async = false;
    protected $notifications = [];

    public function addNotification(array $notification)
    {
        $this->notifications[] = $notification;
    }

    /**
     * Create an HTTP response that represents the object.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function toResponse($request)
    {
        $response = parent::toResponse($request);
        $response->setEncodingOptions(JSON_UNESCAPED_UNICODE);
        return $response;
    }

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $data = parent::toArray($request);
        $this->additional = array_merge($this->additional, [
            'meta' => [
                'success' => $this->success,
                'async' => $this->async,
            ],
            'notifications' => $this->notifications,
        ]);
        return $data;
    }


}
