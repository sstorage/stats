<?php

namespace App\Http\Resources;

use App\Services\MicroserviceClient;

class FilesCollectionResource extends ApiCollectionResource
{
    /**
     * Transform the resource into a JSON array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $result = $this->collection->map->toArray($request)->all();
        foreach ($result as &$item) {
            if (empty($item['uuid'])) {
                continue;
            }
            $item['url'] = $this->getFileUrl($item['uuid']);
        }
        unset($item);
        return $result;
    }

    private function getFileUrl(string $fileId)
    {
        $configPath = 'microservices.public';
        /** @var MicroserviceClient $microserviceClient */
        $microserviceClient = app()->make(MicroserviceClient::class);
        return $microserviceClient->getUrl($configPath, 'file', [$fileId]);
    }
}
