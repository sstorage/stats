<?php

return [

    //Auth microservice routes
    'auth' => [
        'base' => env('AUTH_API_URL', ''),
        'internal' => [
            'user_info' => env('AUTH_API_URL') . '/internal/users/%s',
        ]
    ],

    //Downloader microservice routes
    'downloader' => [
        'base' => env('DOWNLOADER_API_URL', ''),
        'files' => env('DOWNLOADER_API_URL') . '/files',
        'file' => env('DOWNLOADER_API_URL') . '/files/%s',
    ],

    //Uploader microservice routes
    'uploader' => [
        'base' => env('UPLOADER_API_URL', ''),
        'files' => env('UPLOADER_API_URL') . '/files',
        'file' => env('UPLOADER_API_URL') . '/files/%s',
    ],

    'public' => [
        'base' => env('PUBLIC_API_URL', ''),
        'file' => env('PUBLIC_API_URL') . '/files/%s',
    ]
];
